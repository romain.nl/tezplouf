open Js_of_ocaml

let echo x = Printf.ksprintf (fun s -> Firebug.console##log (Js.string s)) x

let http_query ~verb ~url continue =
  let xml_http_request = XmlHttpRequest.create () in
  xml_http_request##_open (Js.string verb) (Js.string url) Js._true;
  xml_http_request##send Js.null;
  xml_http_request##.onload := (
    Dom.handler @@ fun _ ->
    let code = xml_http_request##.status in
    let body = Js.Opt.case xml_http_request##.responseText (fun () -> "") Js.to_string in
    continue code body;
    Js._true
  );
  xml_http_request##.onerror := (
    Dom.handler @@ fun _ ->
    continue 999 "\"HTTP call failed, maybe check CORS\"";
    Js._true
  )

let http_get url = http_query ~verb: "GET" ~url

let on_load f =
  Dom_html.window##.onload := Dom.handler (fun _ -> f (); Js._true)

let get_fragment () =
  let s = Js.to_string Dom_html.window##.location##.hash in
  let len = String.length s in
  if len >= 1 then String.sub s 1 (len - 1) else s

let on_fragment_change f =
  Dom_html.window##.onhashchange :=
    Dom.handler @@ fun _ ->
    f ();
    Js._true

type t = Dom.node Js.t

let add_children parent children =
  List.iter (fun child -> let _: Dom.node Js.t = parent##appendChild child in ()) children

let set ~parent_id nodes =
  Js.Opt.case
    (Dom_html.window##.document##getElementById (Js.string parent_id))
    (fun () -> echo "element not found: %S" parent_id)
  @@ fun parent ->
  let children = parent##.childNodes in
  let rec remove_all i =
    if i >= 0 then
      let child = children##item i in
      Js.Opt.iter child @@ fun child ->
      let _: Dom.node Js.t = parent##removeChild child in
      remove_all (i - 1)
  in
  remove_all (children##.length - 1);
  add_children parent nodes;
  ()

let text s =
  (Dom_html.document##createTextNode (Js.string s) :> t)

let container create children =
  let n = (create Dom_html.document :> t) in
  add_children n children;
  n

let ul = container Dom_html.createUl
let ol = container Dom_html.createOl
let li = container Dom_html.createLi

let a ~href children =
  let a = Dom_html.createA Dom_html.document in
  a##.href := Js.string href;
  add_children a children;
  (a :> t)

let h1 = container Dom_html.createH1

let div = container Dom_html.createDiv

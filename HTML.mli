val echo: ('a, unit, string, unit) format4 -> 'a

val http_get: string -> (int -> string -> unit) -> unit

val on_load: (unit -> unit) -> unit

val get_fragment: unit -> string

val on_fragment_change: (unit -> unit) -> unit

type t

val set: parent_id: string -> t list -> unit

val text: string -> t

val ul: t list -> t

val ol: t list -> t

val li: t list -> t

val a: href: string -> t list -> t

val h1: t list -> t

val div: t list -> t

(* Compile with: dune build main.bc.js

   To test this, start a node with: --rpc-addr localhost:8732 --cors-origin '*'
   (TODO: limit the CORS origin to localhost, but resto refuses)
   Then, open index.html. *)

open HTML

let map l f = List.map f l
let set = set ~parent_id: "main"
let text x = Printf.ksprintf text x

let decode_json s =
  try
    Ezjsonm.value_from_string s
  with Ezjsonm.Parse_error (_, message) ->
    set [ text "Error while parsing JSON: %s" message ];
    failwith message

let is_hash s =
  String.length s >= 15 && (
    let rec loop i =
      if i >= String.length s then
        true
      else
        match s.[i] with
          (* TODO: refine this *)
          | 'a'..'z' | 'A'..'Z' | '0'..'9' ->
              loop (i + 1)
          | _ ->
              false
    in
    loop 0
  )

let has_prefix p s =
  let plen = String.length p in
  let slen = String.length s in
  plen <= slen && String.sub s 0 plen = p

let rec view_json (json: Ezjsonm.value) =
  match json with
    | `Null ->
        text "null"
    | `String s ->
        if is_hash s then
          if has_prefix "B" s then
            a ~href: ("#/chains/main/blocks/" ^ s) [ text "%s" s ]
          else if has_prefix "P" s then
            a ~href: ("#/protocols/" ^ s) [ text "%s" s ]
          else if has_prefix "id" s then
            a ~href: ("#/network/peers/" ^ s ^ "/banned") [ text "%s" s ]
          else if has_prefix "Net" s then
            a ~href: ("#/chains/" ^ s ^ "/blocks") [ text "%s" s ]
          else
            text "%s" s
        else
          text "%s" s
    | `Bool b ->
        text "%b" b
    | `Float f ->
        if Float.is_integer f then
          text "%d" (int_of_float f)
        else
          text "%g" f
    | `A items ->
        ol @@ map items @@ fun item ->
        li [ view_json item ]
    | `O fields ->
        ul @@ map fields @@ fun (name, value) ->
        li [ text "%s = " name; view_json value ]

let view_body body =
  match decode_json body with
    | exception exn ->
        text "invalid JSON: %s" (Printexc.to_string exn)
    | json ->
        view_json json

let update () =
  let path = get_fragment () in
  let path = if path = "" || path = "/" then "/chains/main/blocks/head" else path in
  let path = if path = "" || path.[0] <> '/' then "/" ^ path else path in
  http_get ("http://localhost:8732" ^ path) @@ fun code body ->
  set [
    ul (
      List.map (fun path -> li [ a ~href: ("#" ^ path) [ text "GET %s" path ] ]) [
        "/chains/main/blocks";
        "/chains/main/blocks/head";
        "/chains/main/blocks/genesis";
        "/chains/main/chain_id";
        "/chains/main/checkpoint";
        "/chains/main/invalid_blocks";
        "/chains/main/is_bootstrapped";
        "/chains/main/mempool/filter";
        "/chains/main/mempool/pending_operations";
        "/chains/main/sync_state";
        "/config/network/user_activated_protocol_overrides";
        "/config/network/user_activated_upgrades";
        "/errors";
        "/network/connections";
        "/network/peers";
        "/network/points";
        "/network/self";
        "/network/stat";
        "/protocols";
        "/stats/gc";
        "/stats/memory";
        "/version";
        "/workers/block_validator";
        "/workers/chain_validators";
        "/workers/prevalidators";
      ]
    );
    div [
      h1 [ text "%s" path ];
      div [ text "HTTP %d" code ];
      div [ view_body body ];
    ];
  ]

let () =
  on_load update;
  on_fragment_change update
